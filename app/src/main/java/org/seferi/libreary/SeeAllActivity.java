package org.seferi.libreary;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import java.util.ArrayList;

public class SeeAllActivity extends AppCompatActivity {

    private RecyclerView recViewBooks;
    private recViewBookAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all);

        //To enable the back button next to the app name:

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        // overridePendingTransition(R.anim.slide_in, R.anim.slide_out); //Create an animation (anim) folder in res and pass the animations here

        adapter = new recViewBookAdapter(this, "allBooks");
        recViewBooks = findViewById(R.id.recViewBooks);

        recViewBooks.setAdapter(adapter);
        recViewBooks.setLayoutManager(new LinearLayoutManager(this));


        adapter.setBooks(Utils.getInstance(this).getAllBooks());
    }

    // After enabling the back button next to app name, navigate the user below:
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //overrides the default animation everytime an Activity ends... If you want to override animation across whole app, do it at styles.xml
   /* @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
    }*/
}